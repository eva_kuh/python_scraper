import sys
import scrapy

from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError
from twisted.internet.error import TimeoutError, TCPTimedOutError

from articles.items import ArticleItem


# function to extract number from string (used in comments)
def get_number(s):
    return int(''.join(filter(str.isdigit, s)))


# main class to get articles
class IdnesSpider(scrapy.Spider):
    name = "idnes"
    cnt = 10  # initial value (number of articles)

    def __init__(self, counter=10, *args):
        super(IdnesSpider, self).__init__(*args)
        self.cnt = int(counter)

    def start_requests(self):
        url = 'https://www.idnes.cz/zpravy/archiv?idostrova=idnes'
        yield scrapy.Request(url=url, callback=self.parse, errback=self.error_check)

    # get article from css of the page
    def parse_article(self, response):
        item = ArticleItem()
        item['link'] = response.url,
        item['header'] = response.css('h1::text').get().strip(),
        item['published'] = response.css('span.time-date::attr(content)').get().strip(),
        item['opener'] = response.css('div.opener::text').get().strip(),

        item['paragraphs'] = response.css('div.bbtext p::text').getall(),
        item['comments'] = get_number(response.css('#moot-linkin span::text').get()),
        yield item

    def parse(self, response):
        if self.cnt == 0:
            return

        # error check
        if response.status != 200:
            error_check(self, response.status)
            return

        # get next article
        for article in response.css('div.art'):
            if self.cnt == 0:
                return
            self.cnt -= 1
            next_art = article.css('a.art-link::attr(href)').get()
            if next_art is not None:
                next_art = response.urljoin(next_art)
                yield response.follow(next_art, callback=self.parse_article, errback=self.error_check)

        # get next page
        next_page = response.css('a.ico-right::attr(href)').get()

        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield response.follow(next_page, callback=self.parse, errback=self.error_check)

    def error_check(self, failure):
        # log all failures
        self.logger.error(repr(failure))

        # in case you want to do something special for some errors,
        # you may need the failure's type:

        if failure.check(HttpError):
            # these exceptions come from HttpError spider middleware
            # you can get the non-200 response
            response = failure.value.response
            self.logger.error('HttpError on %s', response.url)

        elif failure.check(DNSLookupError):
            # this is the original request
            request = failure.request
            self.logger.error('DNSLookupError on %s', request.url)

        elif failure.check(TimeoutError, TCPTimedOutError):
            request = failure.request
            self.logger.error('TimeoutError on %s', request.url)