import os
from flask import Flask, request, jsonify
from flask_pymongo import PyMongo



application = Flask(__name__)

application.config["MONGO_URI"] = 'mongodb://' \
                                  + os.environ['MONGODB_USERNAME'] + ':' \
                                  + os.environ['MONGODB_PASSWORD'] + '@' \
                                  + os.environ['MONGODB_HOSTNAME'] + ':27017/' \
                                  + os.environ['MONGODB_DATABASE']

mongo = PyMongo(application)
db = mongo.db
col = db["articles"]


# sort articles by datetime and return n of them
@application.route('/articles/newest')
def newest_articles():
    n = int(request.args.get("number"))
    newest = col.find().sort("published", -1).limit(n)

    # item = {}
    data = []
    for art in newest:
        item = {
            'id': str(art['_id']),
            'link': art['link'],
            'header': art['header'],
            'published': art['published'],
            'opener': art['opener'],
            'paragraphs': art['paragraphs'],
            'comments': art['comments']
        }
        data.append(item)

    return jsonify(
        status=True,
        data=data
    )


# sort articles by comments and return n of them
@application.route('/articles/commented')
def most_commented_articles():
    n = int(request.args.get("number"))
    newest = db.articles.find().sort('comments', -1).limit(n)

    # item = {}
    data = []
    for art in newest:
        item = {
            'id': str(art['_id']),
            'link': art['link'],
            'header': art['header'],
            'published': art['published'],
            'opener': art['opener'],
            'paragraphs': art['paragraphs'],
            'comments': art['comments']
        }
        data.append(item)

    return jsonify(
        status=True,
        data=data
    )


if __name__ == "__main__":
    ENVIRONMENT_DEBUG = os.environ.get("APP_DEBUG", True)
    ENVIRONMENT_PORT = os.environ.get("APP_PORT", 5000)
    application.run(host='0.0.0.0', port=ENVIRONMENT_PORT, debug=ENVIRONMENT_DEBUG)