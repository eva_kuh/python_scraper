# -*- coding: utf-8 -*-


import scrapy
from scrapy.item import Item, Field


class ArticleItem(Item):
    link = Field()
    header = Field()
    published = Field()
    opener = Field()
    paragraphs = Field()
    comments = Field()
