# -*- coding: utf-8 -*-

import pymongo
import logging

from scrapy import settings
from scrapy.exceptions import DropItem
from scrapy.utils import log


class ArticlesPipeline(object):
    def process_item(self, item, spider):
        return item


class MongoPipeline:

    collection_name = 'articles'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri="mongodb://"
                      + crawler.settings.get('MONGO_USERNAME') + ":"
                      + crawler.settings.get('MONGO_PASSWORD') + "@"
                      + crawler.settings.get('MONGO_SERVER') + ":"
                      + crawler.settings.get('MONGO_PORT'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        self.db[self.collection_name].update_one({"link": item["link"]}, {"$set": dict(item)}, True)
        return item
