# Python_scraper
## Introduction

Python scraper which downloads required number of articles from [idnes.cz](https://www.idnes.cz/).
Downloaded articles saves into Mongo database. 

Also contains API to get required number of newest articles and required number of the most commented articles.

Based on [assignment](https://github.com/agrpdev/homeworks/blob/master/BACKEND_PYTHON.md)
## Usage
1. Change the directory to articles_scraper

    ```cd articles```
2. Run the database.
    
    ```sudo docker-compose up --build```
3. Fill this database with the newest articles. 
    If we created idnes.json file before, we need to delete it first.

    - XX - how many newest articles we want to insert into database
    
    ```scrapy crawl idnes -a counter=XX -o idnes.json```  
    
4.  API is available here:
    - to get number of newest articles

    ```localhost/articles/newest?number=XX```
    - to get number of most commented articles

    ```localhost/articles/commented?number=XX```